FROM python:3.8-slim-buster

WORKDIR /app

RUN pip3 install flask gensim==3.8.3 manticoresearch sklearn

COPY . .

EXPOSE 5000

CMD [ "python3", "flask_app.py"]