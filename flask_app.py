from __future__ import print_function
from flask import Flask, request, jsonify

#import pandas as pd
import numpy as np 
import time
import manticoresearch
from manticoresearch.rest import ApiException
from gensim.models import Word2Vec
from gensim.utils import simple_preprocess
from sklearn.metrics.pairwise import cosine_similarity

outer_queries = [('женский',100)] #example of modifying vector search

# Defining the host is optional and defaults to http://127.0.0.1:9308
# See configuration.py for a list of all supported configuration parameters.
configuration = manticoresearch.Configuration(
    host = "http://51.250.9.222:9308"
)
model = Word2Vec.load('word2vec.model')

# Enter a context with an instance of the API client
def send_query(text_query):
    with manticoresearch.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = manticoresearch.UtilsApi(api_client)
        body = "mode=extended&query=SELECT * FROM popularity WHERE MATCH(\'@query " + "*" + text_query + "*"+ "\') ORDER BY query_popularity  DESC, WEIGHT() DESC  LIMIT 100;" # str | 
        print(body)
        try:
            # sql operations
            api_response = api_instance.sql(body.encode('utf-8'))
            #pprint(api_response)
        except ApiException as e:
            print("Exception when calling IndexApi->bulk: %s\n" % e)
    return api_response

def calculate_score(item):
    length_disadvantage = 100 
    max_length = 4
    query = simple_preprocess(item['_source']['query'].lower())
    query_length =  len(simple_preprocess(item['_source']['query'].lower()))
    popularity_score = 20 * item['_source']['query_popularity']
    number_of_short_words = count_shorts(query)
    score = item['_score'] - length_disadvantage * (np.max([query_length,max_length])-max_length) + popularity_score - 10 * number_of_short_words
    return score

def embed_item(query):
    vector = np.zeros(model.wv.vector_size)+0.01
    kwords = 1
    for element in query:
        if element in model.wv.vocab:
            vector += model[element]
            kwords +=1
    vector = vector/kwords
    return vector
            


                              
def count_shorts(query):
    short_words = 0
    for element in query:
        if len(element)<4:
            short_words+=1
    return short_words

def process_queries(resp):
    queries = []
    for item in resp:
        query = item['_source']['query'].lower()
        score = calculate_score(item)
        item_embedding = embed_item(simple_preprocess(query))
        for negative_query in outer_queries:
            score = score + cosine_similarity(embed_item(simple_preprocess(negative_query[0])).reshape(-1,200),item_embedding.reshape(-1,200))*negative_query[1]
        queries.append((query,score))
    return (sorted(queries, key = lambda x: -1*x[1]))

    

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route('/echo', methods=['POST'])
def hello():
   return jsonify(request.json)

@app.route('/recommend', methods=['GET', 'POST'])
def add_message():
    content = request.json
    print(content)
    api_response = send_query(content['query'])
    resp = (api_response['hits']['hits'])
    queries = process_queries(resp)
    queries_text = [query[0] for query in queries[:10]]
    print(queries_text)
    auto_suggest = {"autocomplete":{str(i):autocompletion for i,autocompletion in enumerate(queries_text)}}
    print(auto_suggest)
    return auto_suggest 
        


if __name__ == '__main__':
    app.run(host= '0.0.0.0',debug = False, port=5000)
